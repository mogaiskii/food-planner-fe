import { defineStore } from 'pinia'


const fakeDishes = [
  {
      "id": 1,
      "name": "яйцо пашот",
      "day": "2022-09-19",
      "created_at": "2022-09-03T12:58:18.307Z",
      "updated_at": "2022-09-03T12:58:18.307Z",
      "dish_ingredients": [
          {
              "name": "яйцо",
              "amount": 1
          },
          {
              "name": "хлеб",
              "amount": 0.05
          }
      ],
      "url": "http://localhost:3000/dishes/1.json"
  }
]

function accumulateDishes(dishes) {
  const productsAcc = {}
  for (const dish of dishes) {
    for (const product of dish.dish_ingredients) {
      const key = product.name.toLowerCase()
      if (productsAcc[key] == undefined) productsAcc[key] = 0
      productsAcc[key] += Number.parseFloat(product.amount)
    }
  }
  const result = []
  Object.keys(productsAcc).forEach((key) => {
    result.push({name: key, amount: productsAcc[key]})
  })
  return result
}

export const useDishesStore = defineStore('dishes', {
  state: () => ({
    dishes: fakeDishes
  }),
  getters: {
    byDays(state) {
      return ({start, end}) => {
        const weekdays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
        console.log('days')

        const days = {}
        let currentDate = start
        currentDate.setHours(0, 0, 0, 0)
        const lastDate = end
        lastDate.setHours(0, 0, 0, 0)
        while (currentDate.getTime() <= lastDate.getTime()) {
          const weekday = weekdays[currentDate.getDay()]
          days[currentDate.getTime()] =  {name: weekday, day: currentDate, dishes: []}
          currentDate = new Date((new Date(currentDate.getTime())).setDate(currentDate.getDate() + 1))
          currentDate.setHours(0, 0, 0, 0)
        }

        for (const dish of state.dishes) {
          const date = new Date(dish.day)
          date.setHours(0, 0, 0, 0)

          if (days[date.getTime()] == undefined) {
            const weekday = weekdays[date.getDay()]
            console.warn({name: weekday, day: date, dishes: []})
          } else {
            days[date.getTime()].dishes.push(dish)
          }

        }

        return Object.values(days)
      }
    },
    totalWithDays(state) {
      let start, end
      for (const dish of state.dishes) {
        const date = new Date(dish.day)
        date.setHours(0, 0, 0, 0)
        if (start == undefined || start > date) start = date
        if (end == undefined || end < date) end = date
      }
      return this.byDays({start, end})
    },
    totalProducts(state) {
      return accumulateDishes(state.dishes)
    },
    totalProductsByDates(state) {
      return ({start, end}) => {
        const days = this.byDays({start, end})
        const dishes = []
        for (const day of days) {
          dishes.push(...day.dishes)
        }
        return accumulateDishes(dishes)
      }
    },
    totalProductsByDay(state) {
      return (day) => {
        const requestedDay = this.totalWithDays.find(d => d.day.getTime() == day.getTime())
        if (!requestedDay) return []
        return accumulateDishes(requestedDay.dishes)
      }
    }
  },
  actions: {
    setDishes(dishes) {
      this.dishes = dishes
    },
    setFakeDishes() {
      this.dishes = fakeDishes
    },
    addDish(dish) {
      this.dishes.push(dish)
    },
    removeDish(dish) {
      const index = this.dishes.findIndex(i => i == dish)
      this.dishes.splice(index, 1)
    },
    updateDish(dish, changes) {
      const index = this.dishes.findIndex(i => i == dish)
      for (const key in changes) {
        if (Object.hasOwnProperty.call(changes, key)) {
          const newValue = changes[key];
          this.dishes[index][key] = newValue
        }
      }
    },
    addIngredient(dish, ingredientName) {
      const index = this.dishes.findIndex(i => i == dish)
      this.dishes[index].dish_ingredients.push({name: name, amount: 0, isNew: true})
    },
    updateIngredient(dish, {old, updated}) {
      const index = this.dishes.findIndex(i => i == dish)
      const item = this.dishes[index].dish_ingredients.find(i => i == old)
      Object.keys(updated).forEach(key => {
        item[key] = updated[key]
        if (key == 'isNew') delete item.isNew
      })
    },
    removeIngredient(dish, ingredient) {
      const index = this.dishes.findIndex(i => i == dish)
      const ingredientIndex = this.dishes[index].dish_ingredients.findIndex(i => i == ingredient)
      this.dishes[index].dish_ingredients.splice(ingredientIndex, 1)
    }
  }
})