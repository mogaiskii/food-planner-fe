import axios from "axios";

const devInstance = createInstance("http://localhost:3000");

function createInstance(baseURL){
    return axios.create({
        baseURL,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${localStorage.token}`
        }
    });
}

export default {
    install (app) {
      app.config.globalProperties.$http = devInstance
      console.log('init')
    }
}