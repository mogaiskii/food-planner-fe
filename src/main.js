import { createApp } from 'vue'
import { createPinia } from 'pinia'
import VCalendar from 'v-calendar';

import App from './App.vue'
import router from './router'
import http from './plugins/http.js'
import ClickOutsideDirective from './directives/click_outside.js'

import './assets/main.css'
import 'v-calendar/dist/style.css';
import persistance from './plugins/persistance';

const app = createApp(App)

const pinia = createPinia()

pinia.use(persistance)

app.use(pinia)
app.use(router)
app.use(http)
app.use(VCalendar, {})

app.directive('click-outside', ClickOutsideDirective)

app.mount('#app')
