import store from 'store'

export default {
  setDishes(dishes) {
    store.set('dishes', dishes)
  },
  getDishes() {
    console.log('get persist')
    return store.get('dishes')
  },
  clear() {
    store.clearAll()
  }
}
