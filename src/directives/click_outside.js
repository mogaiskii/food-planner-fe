export default {
  mounted(el, binding, vnode, prevVnode) {
    el.clickOutsideEvent = function(evt) {
      if (!(el == evt.target || el.contains(evt.target))) {
        binding.value(false)
      }
    }
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  beforeUnmount(el, binding, vnode, prevVnode) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  }
}